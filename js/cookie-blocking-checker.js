/**
 * @file
 */

(function ($, window, Drupal, drupalSettings, once) {

  'use strict';

  /**
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the Address auto-complete elements processing.
   */
  Drupal.behaviors.cookieBlockingChecker = {

    attach: function (context, settings) {
      var _this = this;
      once('cookieBlockingChecker', 'html', context).forEach( function (element) {
        _this.checkCookieBlocking();
      });
    },
    checkCookieBlocking: function () {
      const messages = new Drupal.Message();
      if (!this.areCookiesEnabled()) {
        messages.add(drupalSettings.cookie_blocking_checker.message, {type: 'error'});
      }
    },
    areCookiesEnabled: function () {
      if (navigator.cookieEnabled) return true;
      // Create cookie
      document.cookie = "cookietest=1";
      var ret = document.cookie.indexOf("cookietest=") != -1;
      // Delete cookie
      document.cookie = "cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT";
      return ret;
    }
  };

})(jQuery, window, Drupal, drupalSettings, once);

<?php

namespace Drupal\cookie_blocking_checker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Cookie Blocking Checker settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cookie_blocking_checker_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cookie_blocking_checker.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cookie_blocking_checker.settings');
    $message = $config->get('cookies_blocked_message');
    $form['cookies_blocked_message'] = [
      '#type' => 'text_format',
      '#format' => $message['format'] ?? null,
      '#title' => $this->t('Cookies blocked message'),
      '#default_value' => $message['value'] ?? '',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('cookie_blocking_checker.settings')
      ->set('cookies_blocked_message', $form_state->getValue('cookies_blocked_message'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
